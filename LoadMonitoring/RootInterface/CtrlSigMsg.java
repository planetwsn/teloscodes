

public class CtrlSigMsg extends net.tinyos.message.Message {

	/** The default size of this message type (controlsignal) in bytes **/
	public static final int DEFAULT_MESSAGE_SIZE = 3;

	/** The Active Message type associated with this message (no idea)**/
	public static final int AM_TYPE = 143;

	public static final int DATA_REQ = 20;
	public static final int LOAD_MNG = 44;
	public static final int DO_NOTHING = 33;
	public static final int TIVA_ACK = 78;

	/** Create a new CtlSigMsg of size 5 bytes */
	public CtrlSigMsg(){
		super(DEFAULT_MESSAGE_SIZE);
		amTypeSet(AM_TYPE);
	}

	/**
	 * Return a String representation of this message. Includes the
	 * message type name and the non-indexed field values.
	 */
	public String toString(){
		String s = " ";
		try{
			s += "Control Signal type: " + Long.toString(get_type()) + " ";
			s += "Control Signal value: " + Long.toString(get_value());
		}
		catch (Exception e) {}
		return s;
	}

	/////////////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'unique_identifier'
	//    all fields type: int, unsigned
	//    offset (bits): 0
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	 /**
	  * Return the offset(in bytes) of the field 'id'
	  */
	public static int offset_id(){
		 return(0 / 8);
	 }
	 /**
	  * Return the offset(in bits) of the field 'id'
	  */
	 public static int offsetBits_id(){
		 return 0;
	 }
	/**
	 * Return the value (as an int) of the field 'id'
	 */
	public int get_id(){
		 return (int)getUIntBEElement(offsetBits_id(),8);
	}
	/**
	 * Set the value of the field 'id'
	 */
	public void set_id(int value){
		setUIntBEElement(offsetBits_id(),8,value);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'cs_type'
	//    all fields type: int, unsigned
	//    offset (bits): 8
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'cs_type'
	  */
	 public static int offset_type(){
		 return(8 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'cs_type'
	  */
	 public static int offsetBits_type(){
		 return 8;
	 }

	/**
	 * Return the value (as an int) of the field 'cs_type'
	 */
	public int get_type(){
		 return (int)getUIntBEElement(offsetBits_type(),8);
	}

	/**
	 * Set the value of the field 'cs_type'
	 */
	public void set_type(int value){
		setUIntBEElement(offsetBits_type(),8,value);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'value'
	//    all fields type: int, unsigned
	//    offset (bits): 16
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'value'
	  */
	 public static int offset_value(){
		 return(16 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'value'
	  */
	 public static int offsetBits_value(){
		 return 16;
	 }

	/**
	 * Return the value (as an int) of the field 'value'
	 */
	public int get_value(){
		 return (int)getUIntBEElement(offsetBits_value(),8);
	}

	/**
	 * Set the value of the field 'value'
	 */
	public void set_value(int value){
		setUIntBEElement(offsetBits_value(),8,value);
	}
}
