import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.text.DecimalFormat;

public class PowerSigMsg extends net.tinyos.message.Message {

	/** The default size of this message type (powersignalmsg) in bytes **/
	public static final int DEFAULT_MESSAGE_SIZE = 23;

	/** The Active Message type associated with this message;
	 *	 this Active Message type (AM_TYPE) is the id assigned to
	 *  the AM interface in <teloscode>AppC.nc.
	 */
	public static final int AM_TYPE = 143;

	/** Create a new CtlSigMsg of size 5 bytes */
	public PowerSigMsg(){
		super(DEFAULT_MESSAGE_SIZE);
		amTypeSet(AM_TYPE);
	}

	/**
	 * Return a String representation of this message. Includes the
	 * message type name and the non-indexed field values.
	 */
	public String toString(){
		String s = "Message <PowerSigMsg> \n";
		try{
			s += "  [nodeID=0x"+Long.toHexString(get_node())+",\n";
			s += "   vint=0x"+Long.toHexString(get_vi())+",\n";
			s += "   vfrac=0x"+Long.toHexString(get_vf())+",\n";
			s += "   iint=0x"+Long.toHexString(get_ii())+",\n";
			s += "   ifrac=0x"+Long.toHexString(get_if())+",\n";
			s += "   actpi=0x"+Long.toHexString(get_actpi())+",\n";
			s += "   actpf=0x"+Long.toHexString(get_actpf())+",\n";
			s += "   reapi=0x"+Long.toHexString(get_reapi())+",\n";
			s += "   reapf=0x"+Long.toHexString(get_reapf())+",\n";
			s += "   apppi=0x"+Long.toHexString(get_apppi())+",\n";
			s += "   apppf=0x"+Long.toHexString(get_apppf())+",\n";
			s += "   freqi=0x"+Long.toHexString(get_freqi())+",\n";
			s += "   freqf=0x"+Long.toHexString(get_freqf())+",\n";
			s += "   pint=0x"+Long.toHexString(get_pi())+",\n";
			s += "   pfrac=0x"+Long.toHexString(get_pf())+ ", \n";
			s += "   batti=0x"+Long.toHexString(get_bvi())+ ", \n";
			s += "   battf=0x"+Long.toHexString(get_bvf())+ ", \n";
			s += "   soci=0x"+Long.toHexString(get_soci())+ ", \n";
			s += "   socf=0x"+Long.toHexString(get_socv())+ "]\n";
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {}
		return s;
	}
	public double get_volt(){
		return (double)get_vi() + (double)get_vf()/100.0;
	}

	public double get_curr(){
		return (double)get_ii() + (double)get_if()/100.0;
	}

	public double get_ppwr(){
		return (double)get_actpi()-2500 + (double)get_actpf()/100.0;
	}

	public double get_qpwr(){
		return (double)get_reapi()-2500 + (double)get_reapf()/100.0;
	}

	public double get_spwr(){
		return (double)get_apppi()-2500 + (double)get_apppf()/100.0;
	}

	public double get_freq(){
		return (double)get_freqi() + (double)get_freqf()/100.0;
	}

	public double get_ph(){
		return (double)get_pi()-360 + (double)get_pf()/100.0;
	}
	
	public double get_batt(){
		return (double)get_bvi() + (double)get_bvf()/100.0;
	}
	
	public double get_soc(){
		return (double)get_soci() + (double)get_socv()/100.0;
	}

	public void printPacketStringDbl(){
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
                DecimalFormat df = new DecimalFormat("#.00");
                try{
                        System.out.print(dtf.format(now) + "    ");
                        System.out.print(Long.toString(get_node()) + "    ");
                        System.out.print(df.format(get_volt()) + "    ");
                        System.out.print(df.format(get_curr()) + "    ");
                        System.out.print(df.format(get_ppwr()) + "    ");
						System.out.print(df.format(get_qpwr()) + "    ");
						System.out.print(df.format(get_spwr()) + "    ");
						System.out.print(df.format(get_freq()) + "    ");
						System.out.print(df.format(get_ph()) + "     ");
						System.out.print(df.format(get_batt()) + "      ");
						System.out.println(df.format(get_soc());
                }
                catch(ArrayIndexOutOfBoundsException aioobe){
                }
	}

    /////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'node_id'
	//    all fields type: int, unsigned
	//    offset (bits): 0
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////

	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_node(){
		 return false;
	 }

	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_node(){
		 return false;
	 }

	 /**
	  * Return the offset(in bytes) of the field 'node
	  */
	 public static int offset_node(){
		 return(0 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'node'
	  */
	 public static int offsetBits_node(){
		 return 0;
	 }

	/**
	 * Return the value (as an int) of the field 'node'
	 */
	public int get_node(){
		 return (int)getUIntBEElement(offsetBits_node(),8);
	}

	/**
	 * Set the value of the field 'node'
	 */
	public void set_node(int value){
		setUIntBEElement(offsetBits_node(),8,value);
	}

	/**
	 * return the size (in bytes) of the field 'node'
	 */
	public static int size_node(){
		return (8/8);
	}

	/**
	 * Return the size (in bits) of the field 'node'
	 */
	public static int sizeBits_node(){
		return 8;
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'vint'
	//    all fields type: int, unsigned
	//    offset (bits): 8
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////

	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_vi(){
		 return false;
	 }

	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_vi(){
		 return false;
	 }

	 /**
	  * Return the offset(in bytes) of the field 'vi'
	  */
	 public static int offset_vi(){
		 return(8 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'vi'
	  */
	 public static int offsetBits_vi(){
		 return 8;
	 }

	/**
	 * Return the value (as an int) of the field 'vi'
	 */
	public int get_vi(){
		 return (int)getUIntBEElement(offsetBits_vi(),8);
	}

	/**
	 * Set the value of the field 'vi'
	 */
	public void set_vi(int value){
		setUIntBEElement(offsetBits_vi(),8,value);
	}

	/**
	 * return the size (in bytes) of the field 'vi'
	 */
	public static int size_vi(){
		return (8/8);
	}

	/**
	 * Return the size (in bits) of the field 'vi'
	 */
	public static int sizeBits_vi(){
		return 8;
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'vf'
	//    all fields type: int, unsigned
	//    offset (bits): 16
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////

	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_vf(){
		 return false;
	 }

	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_vf(){
		 return false;
	 }

	 /**
	  * Return the offset(in bytes) of the field 'vf'
	  */
	 public static int offset_vf(){
		 return(16 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'vf'
	  */
	 public static int offsetBits_vf(){
		 return 16;
	 }

	/**
	 * Return the value (as an int) of the field 'vf'
	 */
	public int get_vf(){
		 return (int)getUIntBEElement(offsetBits_vf(),8);
	}

	/**
	 * Set the value of the field 'vf'
	 */
	public void set_vf(int value){
		setUIntBEElement(offsetBits_vf(),8,value);
	}

	/**
	 * return the size (in bytes) of the field 'vf'
	 */
	public static int size_vf(){
		return (8/8);
	}

	/**
	 * Return the size (in bits) of the field 'vf'
	 */
	public static int sizeBits_vf(){
		return 8;
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'ii'
	//    all fields type: int, unsigned
	//    offset (bits): 24
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_ii(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_ii(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'ii'
	  */
	 public static int offset_ii(){
		 return(24 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'ii'
	  */
	 public static int offsetBits_ii(){
		 return 24;
	 }
	
	/**
	 * Return the value (as an int) of the field 'ii'
	 */
	public int get_ii(){
		 return (int)getUIntBEElement(offsetBits_ii(),8);
	}
		 
	/**
	 * Set the value of the field 'ii'
	 */
	public void set_ii(int value){
		setUIntBEElement(offsetBits_ii(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'ii'
	 */
	public static int size_ii(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'ii'
	 */
	public static int sizeBits_ii(){
		return 8;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'if'
	//    all fields type: int, unsigned
	//    offset (bits): 32
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_if(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_if(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'if'
	  */
	 public static int offset_if(){
		 return(32 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'if'
	  */
	 public static int offsetBits_if(){
		 return 32;
	 }
	
	/**
	 * Return the value (as an int) of the field 'if'
	 */
	public int get_if(){
		 return (int)getUIntBEElement(offsetBits_if(),8);
	}
		 
	/**
	 * Set the value of the field 'if'
	 */
	public void set_if(int value){
		setUIntBEElement(offsetBits_if(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'if'
	 */
	public static int size_if(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'if'
	 */
	public static int sizeBits_if(){
		return 8;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'actpi'
	//    all fields type: int, unsigned
	//    offset (bits): 40
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_actpi(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_actpi(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'actpi'
	  */
	 public static int offset_actpi(){
		 return(40 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'actpi'
	  */
	 public static int offsetBits_actpi(){
		 return 40;
	 }
	
	/**
	 * Return the value (as an int) of the field 'actpi'
	 */
	public int get_actpi(){
		 return (int)getUIntBEElement(offsetBits_actpi(),16);
	}
		 
	/**
	 * Set the value of the field 'actpi'
	 */
	public void set_actpi(int value){
		setUIntBEElement(offsetBits_actpi(),16,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'actpi'
	 */
	public static int size_actpi(){
		return (16/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'actpi'
	 */
	public static int sizeBits_actpi(){
		return 16;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'actpf'
	//    all fields type: int, unsigned
	//    offset (bits): 56
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_actpf(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_actpf(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'actpf'
	  */
	 public static int offset_actpf(){
		 return(56 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'actpf'
	  */
	 public static int offsetBits_actpf(){
		 return 56;
	 }
	
	/**
	 * Return the value (as an int) of the field 'actpf'
	 */
	public int get_actpf(){
		 return (int)getUIntBEElement(offsetBits_actpf(),8);
	}
		 
	/**
	 * Set the value of the field 'actpf'
	 */
	public void set_actpf(int value){
		setUIntBEElement(offsetBits_actpf(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'actpf'
	 */
	public static int size_actpf(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'actpf'
	 */
	public static int sizeBits_actpf(){
		return 8;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'reapi'
	//    all fields type: int, unsigned
	//    offset (bits): 64
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_reapi(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_reapi(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'reapi'
	  */
	 public static int offset_reapi(){
		 return(64 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'reapi'
	  */
	 public static int offsetBits_reapi(){
		 return 64;
	 }
	
	/**
	 * Return the value (as an int) of the field 'reapi'
	 */
	public int get_reapi(){
		 return (int)getUIntBEElement(offsetBits_reapi(),16);
	}
		 
	/**
	 * Set the value of the field 'reapi'
	 */
	public void set_reapi(int value){
		setUIntBEElement(offsetBits_reapi(),16,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'reapi'
	 */
	public static int size_reapi(){
		return (16/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'reapi'
	 */
	public static int sizeBits_reapi(){
		return 16;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'reapf'
	//    all fields type: int, unsigned
	//    offset (bits): 80
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_reapf(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_reapf(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'reapf'
	  */
	 public static int offset_reapf(){
		 return(80 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'reapf'
	  */
	 public static int offsetBits_reapf(){
		 return 8;
	 }
	
	/**
	 * Return the value (as an int) of the field 'reapf'
	 */
	public int get_reapf(){
		 return (int)getUIntBEElement(offsetBits_reapf(),8);
	}
		 
	/**
	 * Set the value of the field 'reapf'
	 */
	public void set_reapf(int value){
		setUIntBEElement(offsetBits_reapf(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'reapf'
	 */
	public static int size_reapf(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'reapf'
	 */
	public static int sizeBits_reapf(){
		return 8;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'apppi'
	//    all fields type: int, unsigned
	//    offset (bits): 88
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_apppi(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_apppi(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'apppi'
	  */
	 public static int offset_apppi(){
		 return(88 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'apppi'
	  */
	 public static int offsetBits_apppi(){
		 return 88;
	 }
	
	/**
	 * Return the value (as an int) of the field 'apppi'
	 */
	public int get_apppi(){
		 return (int)getUIntBEElement(offsetBits_apppi(),16);
	}
		 
	/**
	 * Set the value of the field 'apppi'
	 */
	public void set_apppi(int value){
		setUIntBEElement(offsetBits_apppi(),16,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'apppi'
	 */
	public static int size_apppi(){
		return (16/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'apppi'
	 */
	public static int sizeBits_apppi(){
		return 16;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'apppf'
	//    all fields type: int, unsigned
	//    offset (bits): 104
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_apppf(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_apppf(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'apppf'
	  */
	 public static int offset_apppf(){
		 return(104 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'apppf'
	  */
	 public static int offsetBits_apppf(){
		 return 104;
	 }
	
	/**
	 * Return the value (as an int) of the field 'apppf'
	 */
	public int get_apppf(){
		 return (int)getUIntBEElement(offsetBits_apppf(),8);
	}
		 
	/**
	 * Set the value of the field 'apppf'
	 */
	public void set_apppf(int value){
		setUIntBEElement(offsetBits_apppf(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'apppf'
	 */
	public static int size_apppf(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'apppf'
	 */
	public static int sizeBits_apppf(){
		return 8;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'freqi'
	//    all fields type: int, unsigned
	//    offset (bits): 112
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_freqi(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_freqi(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'freqi'
	  */
	 public static int offset_freqi(){
		 return(112 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'freqi'
	  */
	 public static int offsetBits_freqi(){
		 return 112;
	 }
	
	/**
	 * Return the value (as an int) of the field 'freqi'
	 */
	public int get_freqi(){
		 return (int)getUIntBEElement(offsetBits_freqi(),8);
	}
		 
	/**
	 * Set the value of the field 'freqi'
	 */
	public void set_freqi(int value){
		setUIntBEElement(offsetBits_freqi(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'freqi'
	 */
	public static int size_freqi(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'freqi'
	 */
	public static int sizeBits_freqi(){
		return 8;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'freqf'
	//    all fields type: int, unsigned
	//    offset (bits): 120
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_freqf(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_freqf(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'freqf'
	  */
	 public static int offset_freqf(){
		 return(120 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'freqf'
	  */
	 public static int offsetBits_freqf(){
		 return 120;
	 }
	
	/**
	 * Return the value (as an int) of the field 'freqf'
	 */
	public int get_freqf(){
		 return (int)getUIntBEElement(offsetBits_freqf(),8);
	}
		 
	/**
	 * Set the value of the field 'freqf'
	 */
	public void set_freqf(int value){
		setUIntBEElement(offsetBits_freqf(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'freqf'
	 */
	public static int size_freqf(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'freqf'
	 */
	public static int sizeBits_freqf(){
		return 8;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'pi'
	//    all fields type: int, unsigned
	//    offset (bits): 128
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_pi(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_pi(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'pi'
	  */
	 public static int offset_pi(){
		 return(128 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'pi'
	  */
	 public static int offsetBits_pi(){
		 return 128;
	 }
	
	/**
	 * Return the value (as an int) of the field 'pi'
	 */
	public int get_pi(){
		 return (int)getUIntBEElement(offsetBits_pi(),16);
	}
		 
	/**
	 * Set the value of the field 'pi'
	 */
	public void set_pi(int value){
		setUIntBEElement(offsetBits_pi(),16,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'pi'
	 */
	public static int size_pi(){
		return (16/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'pi'
	 */
	public static int sizeBits_pi(){
		return 16;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'pf'
	//    all fields type: int, unsigned
	//    offset (bits): 144
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_pf(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_pf(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'pf'
	  */
	 public static int offset_pf(){
		 return(144 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'pf'
	  */
	 public static int offsetBits_pf(){
		 return 144;
	 }
	
	/**
	 * Return the value (as an int) of the field 'pf'
	 */
	public int get_pf(){
		 return (int)getUIntBEElement(offsetBits_pf(),8);
	}
		 
	/**
	 * Set the value of the field 'pf'
	 */
	public void set_pf(int value){
		setUIntBEElement(offsetBits_pf(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'pf'
	 */
	public static int size_pf(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'pf'
	 */
	public static int sizeBits_pf(){
		return 8;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'bvi'
	//    all fields type: int, unsigned
	//    offset (bits): 152
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_bvi(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_bvi(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'bvi'
	  */
	 public static int offset_bvi(){
		 return(152 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'bvi'
	  */
	 public static int offsetBits_bvi(){
		 return 152;
	 }
	
	/**
	 * Return the value (as an int) of the field 'bvi'
	 */
	public int get_bvi(){
		 return (int)getUIntBEElement(offsetBits_bvi(),8);
	}
		 
	/**
	 * Set the value of the field 'bvi'
	 */
	public void set_bvi(int value){
		setUIntBEElement(offsetBits_bvi(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'bvi'
	 */
	public static int size_bvi(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'bvi'
	 */
	public static int sizeBits_bvi(){
		return 8;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'bvf'
	//    all fields type: int, unsigned
	//    offset (bits): 160
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_bvf(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_bvf(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'bvf'
	  */
	 public static int offset_bvf(){
		 return(160 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'bvf'
	  */
	 public static int offsetBits_bvf(){
		 return 160;
	 }
	
	/**
	 * Return the value (as an int) of the field 'bvf'
	 */
	public int get_bvf(){
		 return (int)getUIntBEElement(offsetBits_bvf(),8);
	}
		 
	/**
	 * Set the value of the field 'bvf'
	 */
	public void set_bvf(int value){
		setUIntBEElement(offsetBits_bvf(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'bvf'
	 */
	public static int size_bvf(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'bvf'
	 */
	public static int sizeBits_bvf(){
		return 8;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'soci'
	//    all fields type: int, unsigned
	//    offset (bits): 168
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_soci(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_soci(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'soci'
	  */
	 public static int offset_soci(){
		 return(168 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'soci'
	  */
	 public static int offsetBits_soci(){
		 return 168;
	 }
	
	/**
	 * Return the value (as an int) of the field 'soci'
	 */
	public int get_soci(){
		 return (int)getUIntBEElement(offsetBits_soci(),8);
	}
		 
	/**
	 * Set the value of the field 'soci'
	 */
	public void set_soci(int value){
		setUIntBEElement(offsetBits_soci(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'soci'
	 */
	public static int size_soci(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'soci'
	 */
	public static int sizeBits_soci(){
		return 8;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'socv'
	//    all fields type: int, unsigned
	//    offset (bits): 176
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_socv(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_socv(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'socv'
	  */
	 public static int offset_socv(){
		 return(176 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'socv'
	  */
	 public static int offsetBits_socv(){
		 return 176;
	 }
	
	/**
	 * Return the value (as an int) of the field 'socv'
	 */
	public int get_socv(){
		 return (int)getUIntBEElement(offsetBits_socv(),8);
	}
		 
	/**
	 * Set the value of the field 'socv'
	 */
	public void set_socv(int value){
		setUIntBEElement(offsetBits_socv(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'socv'
	 */
	public static int size_socv(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'socv'
	 */
	public static int sizeBits_socv(){
		return 8;
	}
}

