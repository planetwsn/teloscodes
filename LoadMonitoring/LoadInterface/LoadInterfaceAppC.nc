#include "datastruct.h"

configuration LoadInterfaceAppC {
}

implementation {
  //main components
  components LoadInterfaceC as App;
  components MainC;
  components new TimerMilliC() as Timer0;
  App.Counter -> Timer0;
  App.Boot -> MainC;

#ifdef DEBUG
  components LedsC;
  App.Leds -> LedsC;
#endif
  
  //GPIO components
  components HplMsp430GeneralIOC;
  components HplMsp430InterruptC;

  App.ReadPort -> HplMsp430GeneralIOC.Port23; //For output- GIO2
  App.InPort -> HplMsp430GeneralIOC.Port26; //For input - GIO3
  App.Interrupt -> HplMsp430InterruptC.Port26; //For input - GIO3

  //UART components
  components new Msp430Uart0C() as Uart;
  App.UartStream -> Uart.UartStream;
  App.Resource -> Uart.Resource;
  App.Msp430UartConfigure <- Uart.Msp430UartConfigure;
  
  //Split-control components
  components ActiveMessageC;
  App.AMControl -> ActiveMessageC;

  //Radio Tx components

  components CollectionC;
  components new CollectionSenderC(AM_TIVATRANSMIT) as DataSender;
  App.CollectionControl -> CollectionC;
  App.DataSender -> DataSender;
  App.LowPowerListening -> ActiveMessageC;
  
  //Radio Rx components
  components new AMReceiverC(AM_TIVATRANSMIT);
  App.Receive -> AMReceiverC;
}
