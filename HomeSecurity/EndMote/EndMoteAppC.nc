
#include "datastruct.h"

configuration EndMoteAppC {
}

implementation{
	//fundamental interfaces
	components EndMoteC as App;
	components MainC;
	components ActiveMessageC;
	App.Boot -> MainC;
	App.AMControl -> ActiveMessageC;
	
	//GPIO interfaces
	components HplMsp430GeneralIOC;
	components HplMsp430InterruptC as IntC;
	App.InPort->HplMsp430GeneralIOC.Port23; //For input - GIO2
	App.OutPort->HplMsp430GeneralIOC.Port26; //For output - GIO3
	App.Interrupt->IntC.Port23;	

	/*UART components
	components new Msp430Uart0C() as Uart;
	App.UartStream -> Uart.UartStream;
	App.Resource -> Uart.Resource;
	App.Msp430UartConfigure <- Uart.Msp430UartConfigure;
	*/
	
	//CTP components
	components CollectionC;
	components new CollectionSenderC(AM_WSN) as DataSender;
	App.CollectionControl -> CollectionC;
	App.DataSender -> DataSender;
	App. LowPowerListening -> ActiveMessageC;
	
	components DisseminationC;
	components new DisseminatorC(CtlSig_t, 3);
	App.DisseminationControl -> DisseminationC;
	App.SignalValue -> DisseminatorC;
	
	//Radio Rx components
	components new AMReceiverC(AM_WSN);
	App.Receive -> AMReceiverC;
	
	components LedsC;
	App.Leds -> LedsC;
}
