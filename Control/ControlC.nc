#include "packetdata.h"

module ControlC {
	uses{
		interface Boot;
		interface SplitControl as AMControl;
		interface Leds;
	}
	
	uses{
		//interface StdControl as CollectionControl;
		interface StdControl as DisseminationControl;
		interface DisseminationValue<sig_t> as SignalValue;
		interface LowPowerListening;
	}

}

implementation{

	void setLeds(int value){
		switch(value){
			case 1: call Leds.led0On(); call Leds.led1Off(); call Leds.led2Off(); break;
			case 2: call Leds.led0Off(); call Leds.led1On(); call Leds.led2Off(); break;
			case 3: call Leds.led0Off(); call Leds.led1Off(); call Leds.led2On(); break;
		}
	}

	event void Boot.booted(){
		call AMControl.start();
	}

	event void AMControl.startDone(error_t err){
		if(err == SUCCESS){
			call DisseminationControl.start();
		}
		else{
			call AMControl.start();
		}
	}
	
	event void AMControl.stopDone(error_t err){}

	event void SignalValue.changed(){
		const sig_t* val = call SignalValue.get();
		setLeds(val->value);
	}
}
