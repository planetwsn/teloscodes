#include "packetdata.h"

configuration ControlAppC {
}

implementation{
	components MainC, ControlC as App, LedsC;
	App.Boot -> MainC;
	App.Leds -> LedsC;
	
	components ActiveMessageC;
	App.LowPowerListening -> ActiveMessageC;
	App.AMControl ->ActiveMessageC;

	components DisseminationC;
	components new DisseminatorC(sig_t, 3);
	App.DisseminationControl -> DisseminationC;
	App.SignalValue -> DisseminatorC;
}	
