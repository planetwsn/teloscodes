#include "packetdata.h"
#include "printf.h"

module ControllerC {
	uses{
		interface Boot;
		interface SplitControl as AMControl;		
	}

	uses{
		interface StdControl as DisseminationControl;
		interface DisseminationUpdate<sig_t> as SignalUpdate;
		interface Receive as UartReceive;
	}
}

implementation{
	event void Boot.booted(){
		call AMControl.start();
	}

	event void AMControl.startDone(error_t err){
		if(err == FAIL){
			call AMControl.start();
		}
		else{
			call DisseminationControl.start();
		}
	}
	
	event void AMControl.stopDone(error_t err){}
	
	event message_t* UartReceive.receive (message_t* msg, void* payload, uint8_t length){
		if(length != sizeof(sig_t)) {return msg;}
		else{
			sig_t* val = (sig_t*) payload;
			printf("Msg received from java code serially\n");
			printfflush();
			call SignalUpdate.change(val);
			return msg;
		}
	}
}
