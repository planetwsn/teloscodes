#include "printf.h"

module InterruptTestC{
	uses{
		interface Boot;
		interface Leds;
	}

	uses{
		interface HplMsp430GeneralIO as OutPin;
		interface HplMsp430GeneralIO as InPin;
		interface HplMsp430Interrupt as Interrupt;
	}
}

implementation{
	event void Boot.booted(){
		call OutPin.makeOutput();
		call OutPin.clr();
		printf("Output pin configured!\n");
		printfflush();

		call InPin.makeInput();

		call Interrupt.enable();
		call Interrupt.edge(TRUE);
		printf("Interrupt Pin enabled!\n");
		printfflush();
	}
	async event void Interrupt.fired(){
		call Interrupt.clear();
		printf("Interrupt triggered!");	
		printfflush();	

		call Leds.led0Toggle();
	}
}
